**********************************************************************
*Zusammenfassung MBA
**********************************************************************

**********************************************************************
*Programmname, zu eines Programms
**********************************************************************
REPORT zwwi216mba_test01.

**********************************************************************
*Erstellen von lokalen Datentypen
**********************************************************************
TYPES tv_p TYPE p LENGTH 16 DECIMALS 2.

**********************************************************************
*Deklarationen
**********************************************************************

*  *********************************************************************
*  Innerhalb Programm
*  *********************************************************************
DATA gv_numerischeid TYPE n LENGTH 5.                       " -> 98547, wenn weniger als Länge mit führenden 0 aufgefüllt
DATA gv_date TYPE d.                                        " -> 20181231
DATA gv_time TYPE t.                                        " -> 1031
DATA gv_int TYPE i.                                         " -> INT
DATA gv_decimal TYPE p LENGTH 16 DECIMALS 2.                " -> Länge(16)*2-1 Bytes
DATA gv_decimal2 TYPE tv_p.                                 " -> wie tv_p
DATA gv_string TYPE string.                                 " -> Dynamischer Text
DATA gv_string2 LIKE gv_string.                             " -> Datentyp übernehmen
DATA gv_festerstring TYPE c LENGTH 5.                       " -> Text fester Länge, wird aufgefüllt

"Bei Constanten VALUE Pflicht
CONSTANTS gc_pi TYPE p LENGTH 2 DECIMALS 2 VALUE '3.14'.

*  *********************************************************************
*   Zuweisung in Deklaration mit Value!
*  *********************************************************************
DATA gv_int2 TYPE i VALUE 42.

**********************************************************************
*Zuweisung
**********************************************************************
gv_numerischeid = '8547'.

gv_date = sy-datlo.
gv_date = '20181231'.

gv_time = sy-timlo.
gv_time = '143045'. "hhmmss

gv_int = 43.
gv_decimal = '1.2'.
gv_string = 'Hallo '.
gv_string2 = ' Welt'.
gv_festerstring = 'ABCDE'.

"Wert auf Initial zurücksetzen
CLEAR gv_int2.


**********************************************************************
* Ausgabe
**********************************************************************
"Kettensatz in neuer Zeile
WRITE: / gv_numerischeid,gv_date, gv_time,gv_int,gv_decimal,gv_string,gv_festerstring.

"Kettensatz ohne neue Zeile
WRITE: gv_numerischeid,gv_date, gv_time,gv_int,gv_decimal,gv_string,gv_festerstring.

"Einzelausgabe
WRITE gv_numerischeid.

"Einzelausgabe in neuer Zeile
WRITE / gv_numerischeid.

**********************************************************************
*Diverses
**********************************************************************

*  *********************************************************************
*  Strings verknüpfen / Templates
*  Achtung: Leerzeichen am Ende des Strings werden abgeschnitten
*  *********************************************************************
DATA gv_newstring TYPE string.
gv_newstring = gv_string && ' ' && gv_string2.          " 'HalloWelt' -> Funktioniert nicht
gv_newstring = gv_string && | | && gv_string2 && '!'.   " 'Hallo Welt!' -> Funktioniert
gv_newstring = |{ gv_string } { gv_string2 }!|.         " 'Hallo Welt!' -> Funktioniert
WRITE / gv_newstring.




**********************************************************************
*Verzweigungen
**********************************************************************

IF gv_int = 42.
  WRITE / 'gv_int ist 42'.
ELSEIF gv_int = 43.
  WRITE / 'gv_int ist 43'.
ELSE.
  WRITE / 'gv_int ist nicht 42 und nicht 43'.
ENDIF.

CASE gv_int.
  WHEN 42.
    WRITE / 'gv_int ist 42'.
  WHEN 43.
    WRITE / 'gv_int ist 43'.
  WHEN OTHERS.
    WRITE / 'gv_int ist nicht 42 und nicht 43'.
ENDCASE.

*TODO Klärung
**********************************************************************
* String Funktionen
* Numerische Funktionen
* Schleifen
* Systemfelder
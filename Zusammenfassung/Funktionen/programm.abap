REPORT zwwi216mba_test03.

DATA gv_int TYPE i.


CALL FUNCTION 'Z_WWI216MBA_SOMEFUNCTION'
  EXPORTING
    iv_inputparam1 = 0
    iv_inputparam2 = 0
  IMPORTING
    ev_result      = gv_int
  EXCEPTIONS
    emtpyparam     = 1
    OTHERS         = 2.

WRITE / gv_int.

IF sy-subrc <> 0.
  WRITE / sy-subrc && '- Es ist ein Fehler aufgetreten.'.
ENDIF.
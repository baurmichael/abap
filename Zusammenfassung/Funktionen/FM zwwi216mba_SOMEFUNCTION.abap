FUNCTION Z_WWI216MBA_SOMEFUNCTION
  IMPORTING
    IV_INPUTPARAM1 TYPE I
    IV_INPUTPARAM2 TYPE I
  EXPORTING
    EV_RESULT TYPE I
  EXCEPTIONS
    EMTPYPARAM.

  IF iv_inputparam1 = 0 OR iv_inputparam2 = 0.
    RAISE emtpyparam.
  ENDIF.

  IF iv_inputparam1 = 1 AND iv_inputparam2 = 1.
    ev_result = gv_int.
  ELSE.
    ev_result = iv_inputparam1 + iv_inputparam2.
  ENDIF.

ENDFUNCTION.
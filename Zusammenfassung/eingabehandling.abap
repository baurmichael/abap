REPORT zwwi216mba_test02.

**********************************************************************
*Eingabeparameter
**********************************************************************

PARAMETERS pcarrid TYPE s_carr_id.
PARAMETERS pconnid TYPE s_conn_id DEFAULT '400'.

**********************************************************************
*Dynamischer Defaultwert
**********************************************************************
INITIALIZATION.
    IF sy-langu = 'D'.
        pcarrid = 'LH'.
    ENDIF.

**********************************************************************
*Eingabeprüfung
**********************************************************************

AT SELECTION-SCREEN on pconnid.
    IF pconnid IS INITIAL.
       MESSAGE 'PCONNID ist leer' TYPE 'E'.
    ENDIF.

AT SELECTION-SCREEN.
    IF pcarrid <> 'LH'.
       MESSAGE e000(zsmg) WITH pcarrid.
    ENDIF.

**********************************************************************
*Hauptverarbeitung
**********************************************************************
START-OF-SELECTION.

WRITE: pcarrid, pconnid.
REPORT zwwi216da_exercise_0303.

**********************************************************************
* Deklarationen
**********************************************************************
PARAMETERS pop1 TYPE p LENGTH 16 DECIMALS 2.
PARAMETERS popr TYPE c LENGTH 1.
PARAMETERS pop2 TYPE p LENGTH 16 DECIMALS 2.

DATA gv_result TYPE p LENGTH 16 DECIMALS 2.

**********************************************************************
* Dynamische Wertvorbelegungen
**********************************************************************
INITIALIZATION.

  IF sy-langu = 'E'.
    popr = '+'.
  ELSE.
    popr = '-'.
  ENDIF.


**********************************************************************
* Eingabeprüfungen
**********************************************************************
AT SELECTION-SCREEN ON popr.

  IF popr <> '+' AND popr <> '-' AND popr <> '*' AND popr <> '/' AND popr <> '%' AND popr <> '^'.
    MESSAGE e002(zwwi16) WITH popr.
  ENDIF.

AT SELECTION-SCREEN.

  IF popr = '/' AND pop2 IS INITIAL.
    MESSAGE e003(zwwi16).
  ENDIF.

  IF pop1 IS INITIAL OR popr IS INITIAL OR pop2 IS INITIAL.
*    MESSAGE e000(zwwi16).
    MESSAGE 'Fehler' TYPE 'E'.
  ENDIF.


**********************************************************************
* Hauptverarbeitung
**********************************************************************
START-OF-SELECTION.

  CASE popr.
    WHEN '+'.
      gv_result = pop1 + pop2.
    WHEN '-'.
      gv_result = pop1 - pop2.
    WHEN '*'.
      gv_result = pop1 * pop2.
    WHEN '/'.
      gv_result = pop1 / pop2.
    WHEN '%'.
      CALL FUNCTION 'Z_WWI216DA_CALC_PERCENTAGE'
        EXPORTING
          iv_percentage = pop1
          iv_base       = pop2
        IMPORTING
          ev_value      = gv_result.
    WHEN '^'.
      CALL FUNCTION 'Z_DAAP_CALC_POWER'
        EXPORTING
          iv_base        = pop1
          iv_exponent    = pop2
        IMPORTING
          ev_result      = gv_result
        EXCEPTIONS
          value_too_high = 1
          OTHERS         = 2.
      IF sy-subrc <> 0.
        MESSAGE e004(zwwi16).
      ENDIF.
  ENDCASE.

  WRITE: 'Ergebnis: ', gv_result.
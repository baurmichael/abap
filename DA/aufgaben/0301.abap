REPORT zwwi216da_exercise_0301.

**********************************************************************
* Deklarationen
**********************************************************************
PARAMETERS pname TYPE string.

DATA gv_text TYPE string.

**********************************************************************
* Hauptverarbeitung
**********************************************************************
gv_text = |Hallo { pname }!|.
WRITE: gv_text.
WRITE: / |Hallo { pname }!|.
WRITE: / |Hallo | && pname && |!|.
WRITE: / 'Hallo', pname, '!'.
REPORT zwwi216da_exercise_0202.

* Deklarationen
DATA gv_product_id TYPE n LENGTH 5.
DATA gv_product TYPE string.
DATA gv_price TYPE p LENGTH 16 DECIMALS 2. "16*2-1=31 (29-Vor, 2-Nachkommastellen)
DATA gv_currency TYPE c.

* Wertzuweisungen
gv_product_id = '1'.
gv_product = 'Brot'.
gv_price = '2.49'.
gv_currency = '€'.

* Ausgabe
WRITE: gv_product_id, gv_product, gv_price, gv_currency.
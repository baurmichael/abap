FUNCTION Z_WWI216DA_GET_PRODUCT_INFO
  IMPORTING
    IV_PRODUCT_ID TYPE ZDAAP_E_PRODUCT_ID
  EXPORTING
    EV_PRODUCT TYPE ZDAAP_E_PRODUCT
    EV_PRICE TYPE ZDAAP_E_PRICE
    EV_CURRENCY TYPE ZDAAP_E_CURRENCY
  EXCEPTIONS
    INVALID_ID.



  CLEAR: ev_product, ev_price, ev_currency.

  IF iv_product_id <= 0.
    RAISE invalid_id.
  ENDIF.

  ev_product = 'Brot'.
  ev_price = '2.49'.
  ev_currency = 'EUR'.

ENDFUNCTION.
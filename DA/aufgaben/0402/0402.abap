REPORT zwwi216da_exercise_0402.

PARAMETERS pid TYPE zdaap_e_product_id.

DATA gv_product TYPE zdaap_e_product.
DATA gv_price TYPE zdaap_e_price.
DATA gv_currency TYPE zdaap_e_currency.

CALL FUNCTION 'Z_WWI216DA_GET_PRODUCT_INFO'
  EXPORTING
    iv_product_id = pid
  IMPORTING
    ev_product    = gv_product
    ev_price      = gv_price
    ev_currency   = gv_currency
  EXCEPTIONS
    invalid_id    = 1
    OTHERS        = 2.
IF sy-subrc <> 0.
  CASE sy-subrc.
    WHEN 1.
      MESSAGE e005(zwwi16).
    WHEN OTHERS.
      MESSAGE e007(zwwi16).
  ENDCASE.
ENDIF.

WRITE: / gv_product, gv_price, gv_currency.
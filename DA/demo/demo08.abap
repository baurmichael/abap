REPORT zwwi216da_demo_08.

**********************************************************************
* Selbstständige Deklaration von internen Tabellen
**********************************************************************
DATA gt_flights TYPE STANDARD TABLE OF sflight
 WITH NON-UNIQUE KEY mandt carrid connid fldate currency planetype.

DATA gt_flights2 TYPE TABLE OF sflight. "TYPE TABLE OF <Struktur>
DATA gt_flights3 TYPE ty_flights. "TYPE <Tabellentyp>

**********************************************************************
* Befüllen von internen Tabellen
**********************************************************************
DATA gs_flight TYPE sflight.
gs_flight-carrid = 'LH'.
gs_flight-connid = 0400.
gs_flight-fldate = '20181231'.

gt_flights = VALUE #(
    ( carrid = 'LH' connid = 0400 fldate = sy-datlo )
    ( carrid = 'LH' connid = 0401 fldate = '20180101' )
    ( gs_flight ) ).

gt_flights = VALUE #( BASE gt_flights
    ( gs_flight )
    ( gs_flight ) ).

* for (Flight f : flights ){
*    flights2.add(f);
* }
gt_flights2 = VALUE #( FOR ls_flight IN gt_flights ( ls_flight ) ).

















WRITE 'ende'.
REPORT zwwi216da_demo01.

* Kommentarzeile
WRITE 'Hello World!'. "Kommentar
* Java: System.out.println("Hello World"); //Kommentar

WRITE /.
WRITE 'Hello'.
WRITE 'World'.
WRITE '!'.

* Kettensatz
WRITE: / 'Hello', 'World', '!'.
* Java: int a, b, c;


**********************************************************************
* Definition von lokalen Datentypen
**********************************************************************
TYPES tv_carrier_id TYPE c LENGTH 3.
TYPES tv_decimal TYPE p LENGTH 16 DECIMALS 2. " Länge * 2 - 1 - Anzahl der Nachkommastellen = Anzahl der Vorkommastellen


**********************************************************************
* Deklaration von Datenobjekten
**********************************************************************
* Java: int i; String text; boolean error;
* Java: byte, short, int, long -> ABAP: i
* Java: float, double -> ABAP: p
* Java: char -> ABAP: c
* Java: boolean -> ABAP: -

* ABAP-Standardtypen
DATA gv_i TYPE i. "Java: int
DATA gv_text TYPE string. "Java: String
DATA gv_numbers TYPE n LENGTH 5. "Java: -
DATA gv_date TYPE d. "Java: LocalDateTime
DATA gv_time TYPE t. "Java: LocalDateTime

* Lokale Datentypen
DATA gv_carrier_id TYPE tv_carrier_id. "C-3
DATA gv_decimal1 TYPE tv_decimal. "P-16-2
DATA gv_decimal2 TYPE tv_decimal. "P-16-2

* Globale Datentypen
DATA gv_carrier_id2 TYPE s_carr_id. "C-3

* Verweis
DATA gv_decimal3 LIKE gv_decimal2. "P-16-2

**********************************************************************
* Wertzuweisung
**********************************************************************
* Java: int i = 5;
gv_i = 5.
gv_text = 'Hallo Welt!'.
gv_numbers = '83421'.
gv_date = '20181025'.
gv_time = '163240'.
gv_carrier_id = 'LH'.
gv_decimal1 = '44.51'. "Java: double d = 44.51;
DATA gv_i2 TYPE i VALUE 42.
DATA(gv_carrier_id3) = 'BA'. "C-2

CLEAR gv_i.

**********************************************************************
* Konstanten
**********************************************************************
* Java: final int PI = 3
CONSTANTS gc_pi TYPE p LENGTH 2 DECIMALS 2 VALUE '3.14'. "2 (Länge) * 2 - 1 = 3

WRITE 'ende'.
REPORT zwwi216da_demo_04.

* Deklarationen
DATA gv_text1 TYPE string VALUE 'Hallo'.
DATA gv_text2 TYPE string VALUE 'Welt'.
DATA gv_text3 TYPE string.
DATA gv_text4 TYPE string.

gv_text3 = gv_text1 && | | && gv_text2 && |!|.
gv_text4 = |{ gv_text1 } { gv_text2 }!|.

SPLIT gv_text4 AT | | INTO gv_text1 gv_text2.

WRITE: gv_text3.
WRITE: / gv_text4.
WRITE: / gv_text1.
WRITE: / gv_text2.
WRITE: / to_upper( gv_text1 ).
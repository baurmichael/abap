REPORT zwwi216da_demo_05.

* Deklarationen
PARAMETERS pcarrid TYPE s_carr_id DEFAULT 'LH'.
PARAMETERS pconnid TYPE s_conn_id DEFAULT 0400.


* Dynamische Wertvorbelegung
INITIALIZATION.
  IF sy-langu = 'E'.
    pconnid = 0401.
  ENDIF.


* Eingabeprüfungen
AT SELECTION-SCREEN ON pcarrid.
  IF pcarrid IS INITIAL.
    MESSAGE e007(zwwi16) WITH 'PCARRID'.
  ENDIF.


* Hauptverarbeitung
START-OF-SELECTION.
  WRITE: pcarrid, pconnid.
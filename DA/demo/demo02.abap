REPORT zwwi216da_demo_02.

* Deklarationen
TYPES tv_carrid TYPE c LENGTH 3.

PARAMETERS pcarrid TYPE c LENGTH 3. "ABAP-Standardtyp
PARAMETERS pcarrid2 TYPE tv_carrid. "lokaler Datentyp
PARAMETERS pcarrid3 TYPE tv_carrid. "lokaler Datentyp
PARAMETERS pcarrid4 TYPE s_carr_id. "globaler Datentyp

DATA gv_carrid TYPE tv_carrid.

DATA gv_i1 TYPE i VALUE 4.
DATA gv_i2 TYPE i VALUE 3.
DATA gv_result TYPE p LENGTH 16 DECIMALS 5.

* Arithmetische Funktionen
gv_result = gv_i1 / gv_i2. "+ - * /
gv_result = gv_i1 ** gv_i2.

ADD 5 TO gv_result.
gv_result = gv_result - 1.

gv_result = abs( -4 ). "4
gv_result = round( val = '1.2775' dec = 2 ). "1.28

* Ausgabe
WRITE: gv_result.
WRITE: / pcarrid.
WRITE: 'Hello World!'(abc).
WRITE: text-abc.

WRITE: / sy-uname.
WRITE: / sy-mandt.
WRITE: / sy-langu.
WRITE: / sy-timlo.
WRITE: / sy-datlo.
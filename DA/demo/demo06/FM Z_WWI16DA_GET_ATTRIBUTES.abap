FUNCTION Z_WWI216DA_GET_ATTRIBUTES
  EXPORTING
    EV_CARRID TYPE S_CARR_ID
    EV_CONNID TYPE S_CONN_ID
  EXCEPTIONS
    NO_DATA.



  CLEAR: ev_carrid, ev_connid.

  IF gv_carrid IS INITIAL OR gv_connid IS INITIAL.
    RAISE no_data.
  ENDIF.

  ev_carrid = gv_carrid.
  ev_connid = gv_connid.

ENDFUNCTION.
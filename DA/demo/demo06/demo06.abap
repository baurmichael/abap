REPORT zwwi216da_demo_06.

**********************************************************************
* Deklarationen
**********************************************************************
PARAMETERS pcarrid TYPE s_carr_id.
PARAMETERS pconnid TYPE s_conn_id.

DATA gv_carrid2 TYPE c LENGTH 3.
DATA gv_connid2 TYPE s_conn_id.

**********************************************************************
* Hauptverarbeitung
**********************************************************************
CALL FUNCTION 'Z_WWI216DA_SET_ATTRIBUTES'
  EXPORTING
    iv_carrid = pcarrid
    iv_connid = pconnid.

* ...

gv_carrid2 = 'AA'.
gv_connid2 = 5013.

CALL FUNCTION 'Z_WWI216DA_GET_ATTRIBUTES'
  IMPORTING
    ev_carrid = gv_carrid2
    ev_connid = gv_connid2
  EXCEPTIONS
    no_data   = 1
    OTHERS    = 2.
IF sy-subrc <> 0.
  CASE sy-subrc.
    WHEN 1.
      MESSAGE e000(zwwi16) WITH 'CARRID' 'CONNID'.
    WHEN 2.
      MESSAGE 'OTHERS' TYPE 'E'.
  ENDCASE.
ENDIF.

WRITE: / gv_carrid2, gv_connid2.
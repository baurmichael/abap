REPORT zwwi216da_demo_07.

**********************************************************************
* Definition lokaler Strukturtypen
**********************************************************************
TYPES: BEGIN OF ts_connection,
         carrid   TYPE s_carr_id,
         connid   TYPE s_conn_id,
         cityfrom TYPE s_from_cit,
         cityto   TYPE s_to_city,
         fltime   TYPE s_fltime,
       END OF ts_connection.

TYPES: BEGIN OF ts_flight,
         carrid TYPE s_carr_id,
         connid TYPE s_conn_id,
         fldate TYPE s_date,
       END OF ts_flight.

TYPES: BEGIN OF ts_connection_flight,
         carrid   TYPE s_carr_id,
         connid   TYPE s_conn_id,
         cityfrom TYPE s_from_cit,
         cityto   TYPE s_to_city,
         fltime   TYPE s_fltime,
         fldate   TYPE s_date,
       END OF ts_connection_flight.


**********************************************************************
* Deklaration von Strukturen
**********************************************************************
DATA gs_connection TYPE ts_connection. "lokaler Strukturtyp
DATA gs_connection2 TYPE zwwi116da_s_connection. "globaler Strukturtyp
DATA gs_connection3 LIKE gs_connection2. "Verweis
DATA gs_flight TYPE ts_flight.
DATA gs_connection_flight TYPE ts_connection_flight.

**********************************************************************
* Befüllen von Strukturen
**********************************************************************
gs_connection2 = VALUE #( carrid = 'LH' connid = 0400 cityfrom = 'FRANKFURT' ).
gs_connection2 = VALUE #( BASE gs_connection2 cityto = 'NEW YORK' ).

DATA(gs_connection4) = VALUE ts_connection( carrid = 'LH' connid = 0401 ).

**********************************************************************
* Zugriff auf Strukturkomponenten
**********************************************************************
gs_connection-cityto = 'BERLIN'.
gs_flight-carrid = 'LH'.
gs_flight-connid = 0400.
gs_flight-fldate = sy-datlo.

WRITE: / gs_connection-carrid.

**********************************************************************
* Kopieren von Strukturen
**********************************************************************
"gs_connection2 = gs_connection.

gs_connection_flight = CORRESPONDING #( gs_connection2 ).
gs_connection_flight = CORRESPONDING #( BASE ( gs_connection_flight ) gs_flight ).






















WRITE 'ende'.
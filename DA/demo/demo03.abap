REPORT zwwi216da_demo_03.

* Deklarationen
PARAMETERS pcarrid TYPE s_carr_id. "Fluggesellschaft Bsp. LH
PARAMETERS pconnid TYPE s_conn_id. "Verbindungsnummer Bsp. 0400

* Kontrollstrukturen: Verzweigungen
IF pcarrid IS NOT INITIAL.
  WRITE: 'PCARRID ist nicht initial'.
ENDIF.

IF pcarrid = 'LH' AND ( pconnid = 0400 OR pconnid = 0402 ). "> < = >= <= <>
  WRITE: 'FRANKFURT -> NEW YORK'.
ELSE.
  WRITE: 'nicht'.
ENDIF.

CASE pconnid.
  WHEN 0400 OR 0402.
    WRITE: 'FRANKFURT -> NEW YORK'.
  WHEN 0401.
    WRITE: 'NEW YORK -> FRANKFURT'.
  WHEN OTHERS.
    WRITE: 'keine Ahnung'.
ENDCASE.

* Kontrollstrukturen: Schleifen
"Java: for-Schleife
DO 10 TIMES.
  WRITE: / sy-index.
ENDDO.

"Java: while-Schleife
WHILE sy-index <= 10.
  WRITE: / sy-index.
ENDWHILE.

"Java: do/while-Schleife
DO.
  WRITE: / sy-index.
  IF sy-index = 10.
    EXIT.
  ENDIF.
ENDDO.

WRITE 'ende'.